
import cv2
from enum import IntFlag

import numpy as np


class Mode(IntFlag):
    NONE = -1
    SELECT = 0


class Pen:
    def __init__(self):
        self._scissors = cv2.segmentation.IntelligentScissorsMB()
        self._img = None
        self._mode = Mode.NONE
        self._points = []
        self._contours = []

    def apply(self, img):
        self._img = img.copy()
        factor = 1
        if self._img.shape[0] > 1000 or self._img.shape[1] > 1000:
            factor = 1000 / max(self._img.shape[:2])
            self._img = cv2.resize(self._img, (0, 0), fx=factor, fy=factor)
        self._canvas = self._img.copy()
        self._scissors.applyImage(self._canvas)
        key = -1
        cv2.namedWindow("Image", cv2.WINDOW_AUTOSIZE)
        cv2.setWindowTitle("Image", "L click: add keypoint. R click: remove keypoint. Enter: close polygon and exit.")
        cv2.setMouseCallback("Image", self._callback, param="test")
        while key != 13:
            cv2.imshow("Image", self._img)
            key = cv2.waitKey(1)
        self._scissors.buildMap(self._points[-1])
        self._contours[-1] = self._scissors.getContour(self._points[0])
        contours = np.concatenate(self._contours, axis=0)
        # scale the contour back to normalized size
        contours = contours / [[self._canvas.shape[:2]]]
        self._mode = Mode.NONE
        self._contours = []
        self._points = []
        self._img = None
        cv2.destroyWindow("Image")
        return contours

    def _callback(self, event, x, y, flags, params=None):
        if self._img is None:
            return
        if event == cv2.EVENT_LBUTTONDOWN:
            self._mode = Mode.SELECT
            self._points.append((x, y))
            self._scissors.buildMap(self._points[-1])

        elif event == cv2.EVENT_RBUTTONDOWN:
            if self._mode == Mode.SELECT:
                if self._contours:
                    self._contours.pop()
                    self._points.pop()
                if self._contours:
                    self._scissors.buildMap(self._points[-1])
                else:
                    self._mode = Mode.NONE
                    self._img = self._canvas.copy()
        elif event == cv2.EVENT_MOUSEMOVE:
            if self._mode == Mode.SELECT:
                if len(self._contours) < len(self._points):
                    self._contours.append(self._scissors.getContour((x, y)))
                else:
                    self._contours[-1] = self._scissors.getContour((x, y))
        if self._contours:
            self._img = cv2.polylines(self._canvas.copy(), self._contours, False, (0, 255, 0), 2, cv2.LINE_AA)
