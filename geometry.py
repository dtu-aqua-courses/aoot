from typing import Sequence

import numpy as np


def intersection(vx1: float, vy1: float, x1: float, y1: float, vx2: float, vy2: float, x2: float, y2: float) -> \
Sequence[float]:
    """
    Finds the intersection point between two non-parallel lines.
    :param vx1: The x gradient of the first line.
    :param vy1: The y gradient of the first line.
    :param x1: The x coordinate of a point lying on the first line.
    :param y1: The y coordinate of a point lying on the first line.
    :param vx2: The x gradient of the second line.
    :param vy2: The y gradient of the second line.
    :param x2: The x coordinate of a point lying on the second line.
    :param y2: The y coordinate of a point lying on the second line.
    :return: The point where the two lines intersect (x, y).
    """
    A = np.array([[vx1, -vx2], [vy1, -vy2]])
    b = np.array([x2 - x1, y2 - y1])
    t = np.linalg.solve(A, b)
    return float(x1 + t[0] * vx1), float(y1 + t[0] * vy1)


def estimate_focal_length(vp1: Sequence[float], vp2: Sequence[float]) -> float:
    """
    Given two orthogonal 2D vanishing points, estimate the focal length.
    :param vp1: non-homogeneous form vanishing point 1
    :param vp2: non-homogeneous form vanishing point 2
    :return: focal length, same units as inputs.
    """
    # Convert vanishing points to homogeneous coordinates
    vp1 = to_homogeneous(vp1)
    vp2 = to_homogeneous(vp2)

    # Formulate the orthogonality constraint
    # vp1 = [x1, y1, 1], vp2 = [x2, y2, 1]
    # Orthogonality constraint: x1 * x2 + y1 * y2 + f^2 = 0
    A = vp1[0] * vp2[0] + vp1[1] * vp2[1]

    # Solve for the focal length
    if A >= 0:
        raise ValueError("The vanishing points do not form orthogonal lines.")

    f_squared = -A
    f = np.sqrt(f_squared)

    return f


def to_homogeneous(vp: Sequence[float]) -> np.ndarray:
    """
    Simple
    :param vp: non-homogeneous point.
    :return: homogeneous representation of point
    """
    return np.array([*vp, 1.0])


def intercept_y0(vx: float, vy: float, x: float, y: float) -> Sequence[float]:
    """
    Finds the intersection point between a non-horizontal line and the x-axis (y = 0)
    :param vx: unit vector of the line in the x direction
    :param vy: unit vector of the line in the y direction
    :param x: x coordinate of point lying on the line
    :param y:  y coordinate of point lying on the line
    :return:  x and y coordinate of the point where line intersects with the x-axis
    """
    if x == 0:
        return y
    return x - (y * vx / vy), 0


def define_parallel_line(vx: float, vy: float, x: float, y: float, d: float) -> Sequence[float]:
    """
    Define a line that is parallel to an input line but offset by a constant distance.
    :param vx: unit vector of the line in the x direction
    :param vy: unit vector of the line in the y direction
    :param x: x coordinate of point lying on the line
    :param y: y coordinate of point lying on the line
    :param d: constant distance to offset the line by
    :return: vx, vy, x, & y descriptors of the offset line.
    """
    # Calculate the magnitude of the direction vector
    magnitude = np.sqrt(vx ** 2 + vy ** 2)

    # Calculate the unit perpendicular vector
    perp_unit_vector = np.array([-vy, vx]) / magnitude

    # Offset the point (x, y) by distance d using the perpendicular unit vector
    x_new = x + d * perp_unit_vector[0]
    y_new = y + d * perp_unit_vector[1]

    return vx, vy, x_new, y_new


def calculate_offset_point(vx: float, vy: float, x: float, y: float, d: float) -> Sequence[float]:
    """
    Find the point on a line offset by a constant distance.
    :param vx: unit vector of the line in the x direction
    :param vy: unit vector of the line in the y direction
    :param x: x coordinate of point lying on the line
    :param y: y coordinate of point lying on the line
    :param d: constant distance to offset the line by
    :return: x, & y coordinates of the point on the line a distance d from the input point
    """
    # Calculate the new point offset by distance d along the direction vector (vx, vy)
    x_new = x + d * vx
    y_new = y + d * vy
    return x_new, y_new
